class Category < ActiveRecord::Base
  acts_as_nested_set
  include TheSortableTree::Scopes
  #attr_accessible :title, :parent_id
  attr_accessible :title, :parent_id, :lft, :rgt, :depth
end
