class HomeController < ApplicationController

  skip_before_filter :authenticate_user!#, :only=>[:set_lang]

  def index

  end

  def registration_entrance

  end
  def registration_redirect
     session['role_of_visitor']= params['id'].to_i

     redirect_to new_user_registration_path
  end


  def set_lang

    locale = params[:locale]
    locale= 'en' unless ['ar', 'en' ].include?(locale)


    I18n.locale = locale

    #raise 'unsupported locale' unless ['ar', 'en' ].include?(locale)
    #current_user.locale = locale
    #current_user.save
    redirect_to :back
  end


end